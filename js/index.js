function checkForm(event) {
  // ngan reload
  event.preventDefault();
  let arrValue = document.querySelectorAll(
    "#contactForm input,#contactForm textarea"
  );
  const infoUser = {};
  for (let item of arrValue) {
    let { id, value } = item;
    infoUser[id] = value;
  }
  let active = true;
  active =
    checkRequired(infoUser.name, "name_warning") &
    checkRequired(infoUser.phone, "phone_warning") &
    checkRequired(infoUser.summary, "summary_warning") &
    checkPhoneNumber(infoUser.phone, "phone_warning");
  if (active) {
    document.getElementById("successSubmit").innerHTML =
      "Để lại thông tin thành công. Carton SGN sẽ sớm liên hệ với bạn.";
    document.getElementById("contactForm").reset();
  }
}
