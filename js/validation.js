function checkRequired(value, idWarning) {
  if (value == "") {
    document.getElementById(idWarning).innerHTML = "Vui lòng không để trống";
    return false;
  } else {
    document.getElementById(idWarning).innerHTML = "";
    return true;
  }
}

function checkPhoneNumber(value, idWarning) {
  let regexPhone = /^(?!.*[^\d+])(84|0[35789])(\d{8})\b/;
  if (!regexPhone.test(value) && value != "") {
    document.getElementById(idWarning).innerHTML = "Số điện thoại không hợp lệ";
    return false;
  } else if (regexPhone.test(value) && value != "") {
    document.getElementById(idWarning).innerHTML = "";
    return true;
  }
}
